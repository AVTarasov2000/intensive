package ru.abdyabdya.intensive.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.abdyabdya.intensive.model.ApplicationUser;
import ru.abdyabdya.intensive.service.ApplicationUserService;

import java.util.UUID;

@RestController
//@Secured({ADMIN_DP, ADMIN_APP, ADMIN_IS})
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminEndpoint {

    private final ApplicationUserService applicationUserRepository;

    @PostMapping
    public ResponseEntity<ApplicationUser> newClient(
            @RequestBody ApplicationUser user
    ){
        return new ResponseEntity(applicationUserRepository.newClient(user), HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity<ApplicationUser> updateClient(
            @RequestBody ApplicationUser user
    ){
        if(user.getId() == null){
            throw new IllegalArgumentException("id is required");
        }
        return new ResponseEntity(applicationUserRepository.updateClient(user), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClient(
            @PathVariable UUID id
            ){
        applicationUserRepository.deleteClient(id);
        return ResponseEntity.ok().build();
    }

}
