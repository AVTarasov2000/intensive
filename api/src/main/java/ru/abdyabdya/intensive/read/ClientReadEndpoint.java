package ru.abdyabdya.intensive.read;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.abdyabdya.intensive.read.fasade.ClientApplierFacade;
import ru.abdyabdya.intensive.read.dto.ClientDto;


@RestController
//@Secured({ADMIN_DP, ADMIN_APP, ADMIN_IS})
@RequiredArgsConstructor
@RequestMapping("/client")
public class ClientReadEndpoint {

    private final ClientApplierFacade clientApplierFacade;

    @GetMapping("/{eventId}")
    public ResponseEntity<ClientDto> findbyEventId(@PathVariable Long eventId){
        return new ResponseEntity(clientApplierFacade.getClient(eventId), HttpStatus.OK);
    }

}
