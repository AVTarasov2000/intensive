package ru.abdyabdya.intensive.read;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.abdyabdya.intensive.model.ContactTemplate;
import ru.abdyabdya.intensive.service.ContactTemplateService;

import java.util.List;

@RestController
//@Secured({ADMIN_DP, ADMIN_APP, ADMIN_IS})
@RequiredArgsConstructor
@RequestMapping("/contact/template")
public class ContactTemplateReadEndpoint {

    private final ContactTemplateService contactTemplateService;

    @GetMapping("/all")
    public ResponseEntity<List<ContactTemplate>> findAll(){
        return new ResponseEntity(contactTemplateService.getAll(), HttpStatus.OK);
    }
}
