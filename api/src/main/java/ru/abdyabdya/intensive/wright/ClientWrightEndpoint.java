package ru.abdyabdya.intensive.wright;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.Publisher;
import ru.abdyabdya.intensive.wright.events.Event;
import ru.abdyabdya.intensive.wright.events.client.CreateClient;
import ru.abdyabdya.intensive.wright.events.client.DeleteClient;
import ru.abdyabdya.intensive.wright.events.client.UpdateClient;

@RestController
//@Secured({ADMIN_DP, ADMIN_APP, ADMIN_IS})
@RequiredArgsConstructor
@RequestMapping("/client")
public class ClientWrightEndpoint {

    private final Publisher publisherService;

    @PostMapping
    public ResponseEntity newClient(
            @RequestBody CreateClient client
    ){
        Command res = publisherService.publish(client);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }

    @PatchMapping
    public ResponseEntity updateClient(
            @RequestBody UpdateClient client
    ){
        Command res = publisherService.publish(client);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }

    @DeleteMapping()
    public ResponseEntity deleteClient(
            @RequestBody DeleteClient deleteClient
            ){
        Command res = publisherService.publish(deleteClient);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }

}
