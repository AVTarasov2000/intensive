package ru.abdyabdya.intensive.wright;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.Publisher;
import ru.abdyabdya.intensive.wright.events.Event;
import ru.abdyabdya.intensive.wright.events.housing.AttachPlace;
import ru.abdyabdya.intensive.wright.events.housing.CancelPlaceReserving;
import ru.abdyabdya.intensive.wright.events.housing.ChangePlaceReserving;
import ru.abdyabdya.intensive.wright.events.housing.ReservePlace;


@RestController
//@Secured({ADMIN_DP, ADMIN_APP, ADMIN_IS})
@RequiredArgsConstructor
@RequestMapping("/place")
public class HousingWrightEndpoint {

    private final Publisher publisherService;

    @PostMapping("/reserve")
    public ResponseEntity reservePlace(
            @RequestBody ReservePlace reservePlace
    ){
        Command res = publisherService.publish(reservePlace);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }
    @PostMapping("/reserve/change")
    public ResponseEntity changeReservation(
            @RequestBody ChangePlaceReserving reservePlace
    ){
        Command res = publisherService.publish(reservePlace);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }
    @PostMapping("/reserve/cancel")
    public ResponseEntity cancelReservation(
            @RequestBody CancelPlaceReserving reservePlace
    ){
        Command res = publisherService.publish(reservePlace);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }
    @PostMapping("/attach")
    public ResponseEntity attachPlace(
            @RequestBody AttachPlace reservePlace
    ){
        Command res = publisherService.publish(reservePlace);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }
}
