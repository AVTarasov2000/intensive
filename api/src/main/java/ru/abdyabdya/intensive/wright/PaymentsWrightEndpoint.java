package ru.abdyabdya.intensive.wright;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.Publisher;
import ru.abdyabdya.intensive.wright.events.Event;
import ru.abdyabdya.intensive.wright.events.payment.AcceptPayment;


@RestController
//@Secured({ADMIN_DP, ADMIN_APP, ADMIN_IS})
@RequiredArgsConstructor
@RequestMapping("/payment")
public class PaymentsWrightEndpoint {

    private final Publisher publisherService;

    @PostMapping("/accept")
    public ResponseEntity acceptPayment(
            @RequestBody AcceptPayment payment
            ){
        Command res = publisherService.publish(payment);
        if (res instanceof Event) return ResponseEntity.ok(res);
        else return ResponseEntity.ok().build();
    }
}
