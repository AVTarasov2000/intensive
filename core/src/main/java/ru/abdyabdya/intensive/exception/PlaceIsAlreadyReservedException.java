package ru.abdyabdya.intensive.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static java.lang.String.format;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaceIsAlreadyReservedException extends IntensiveException{
    private String placeCode;
    
    @Override
    public String getMessage() {
        return format("{placeCode:%s}", placeCode);
    }
}
