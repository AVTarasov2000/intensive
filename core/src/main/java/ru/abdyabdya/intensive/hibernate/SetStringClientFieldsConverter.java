package ru.abdyabdya.intensive.hibernate;

import ru.abdyabdya.intensive.wright.events.client.ClientField;

import javax.persistence.Converter;

@Converter
public class SetStringClientFieldsConverter extends SetStringConverter<ClientField> {
    @Override
    String itemToString(ClientField item) {
        return item.name();
    }
    @Override
    ClientField stringToItem(String item) {
        return ClientField.valueOf(item);
    }

}
