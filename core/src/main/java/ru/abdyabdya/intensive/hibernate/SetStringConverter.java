package ru.abdyabdya.intensive.hibernate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Converter
public abstract class SetStringConverter<T> implements AttributeConverter <Set<T>, String> {

    private static final String DELIMITER = ",";

    abstract String itemToString(T item);
    abstract T stringToItem(String item);

    @Override
    public String convertToDatabaseColumn(Set<T> set) {
        if (set == null || set.isEmpty())
            return "";
        return set.stream()
                .map(this::itemToString)
                .collect(Collectors.joining(DELIMITER));
    }

    @Override
    public Set<T> convertToEntityAttribute(String joined) {
        if (joined.isBlank()){
            return Collections.emptySet();
        }
        return Stream.of(joined.split(DELIMITER))
                .map(this::stringToItem)
                .collect(Collectors.toSet());
    }
}
