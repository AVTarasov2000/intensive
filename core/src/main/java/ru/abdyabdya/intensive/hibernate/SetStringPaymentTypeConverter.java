package ru.abdyabdya.intensive.hibernate;

import ru.abdyabdya.intensive.model.PaymentType;

import javax.persistence.Converter;

@Converter
public class SetStringPaymentTypeConverter extends SetStringConverter<PaymentType> {

    private static final String DELIMITER = ",";

    @Override
    String itemToString(PaymentType item) {
        return item.name();
    }
    @Override
    PaymentType stringToItem(String item) {
        return PaymentType.valueOf(item);
    }
}
