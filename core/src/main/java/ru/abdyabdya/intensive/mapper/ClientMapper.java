package ru.abdyabdya.intensive.mapper;

import org.mapstruct.Mapper;
import ru.abdyabdya.intensive.mapper.config.IgnoreUnmappedMapperConfig;
import ru.abdyabdya.intensive.wright.model.contact.ClientInformation;
import ru.abdyabdya.intensive.read.dto.ClientInformationDto;

@Mapper(config = IgnoreUnmappedMapperConfig.class,
        uses = {ContactMapper.class})
public interface ClientMapper {
    ClientInformationDto toDto(ClientInformation clientInformation);

}
