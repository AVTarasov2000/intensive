package ru.abdyabdya.intensive.mapper;

import org.mapstruct.Mapper;
import ru.abdyabdya.intensive.mapper.config.IgnoreUnmappedMapperConfig;
import ru.abdyabdya.intensive.wright.model.contact.Contact;
import ru.abdyabdya.intensive.read.dto.ContactDto;

@Mapper(config = IgnoreUnmappedMapperConfig.class)
public interface ContactMapper {
    ContactDto toDto(Contact clientInformation);

}
