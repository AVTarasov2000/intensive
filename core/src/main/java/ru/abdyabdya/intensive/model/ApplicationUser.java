package ru.abdyabdya.intensive.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(of = "id")
public class ApplicationUser {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;
    @Column
    private String login;
    @Column
    private String password;
    @Column
    private String email;
    @Column
    private String phone;

}
