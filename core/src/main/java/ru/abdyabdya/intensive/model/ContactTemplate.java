package ru.abdyabdya.intensive.model;

import lombok.*;
import ru.abdyabdya.intensive.wright.model.contact.ContactType;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EqualsAndHashCode(of = "id")
public class ContactTemplate {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;
    @Enumerated(EnumType.STRING)
    private ContactType value;
    @Column
    private String regexp;
    @Column
    private String example;
}
