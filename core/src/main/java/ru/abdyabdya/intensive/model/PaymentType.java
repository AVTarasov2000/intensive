package ru.abdyabdya.intensive.model;

import lombok.Getter;

public enum PaymentType {
    HOUSING("Оплата за проживание"),
    FOOD("Оплата за питание"),
    PARTICIPATION("Оплата за участие");
    @Getter
    private final String description;

    PaymentType(String description) {
        this.description = description;
    }
}
