package ru.abdyabdya.intensive.read.appliers;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import ru.abdyabdya.intensive.ApplierContainer;
import ru.abdyabdya.intensive.annotations.Applier;
import ru.abdyabdya.intensive.mapper.ClientMapper;
import ru.abdyabdya.intensive.read.dto.ClientDto;
import ru.abdyabdya.intensive.wright.events.client.ClientField;
import ru.abdyabdya.intensive.wright.events.client.CreateClient;
import ru.abdyabdya.intensive.wright.events.client.DeleteClient;
import ru.abdyabdya.intensive.wright.events.client.UpdateClient;
import ru.abdyabdya.intensive.wright.events.payment.AddPaymentExpectation;
import ru.abdyabdya.intensive.wright.model.contact.ClientInformation;

@Service
@RequiredArgsConstructor
public class ClientApplier extends ApplierContainer<ClientDto> {

    private final ClientMapper clientMapper;
    private final ClientEnumMappedProcessor clientEnumMappedProcessor;

    @Applier
    public ClientDto apply(ClientDto object, @NonNull CreateClient command) {
        return ClientDto.builder()
                .eventId(command.getEventId())
                .clientInformationDto(
                clientMapper.toDto(command.getClientInformation())).build();
    }

    @Applier
    public ClientDto apply(@NonNull ClientDto object, @NonNull UpdateClient command) {
        for (ClientField clientField : command.getClientFields()) {
            clientEnumMappedProcessor.get(clientField.name())
                    .accept(object, clientMapper.toDto(command.getClientInformation()));
        }
        return object;
    }

    @Applier
    public ClientDto apply(@NonNull ClientDto object, @NonNull DeleteClient command) {
        return null;
    }

    @Applier
    public ClientDto apply(@NonNull ClientDto object, @NonNull AddPaymentExpectation command) {
        return object;
    }

    @Override
    public Class<ClientDto> getApplyingClass() {
        return ClientDto.class;
    }
}
