package ru.abdyabdya.intensive.read.appliers;

import org.springframework.stereotype.Component;
import ru.abdyabdya.intensive.EnumMappedProcessorService;
import ru.abdyabdya.intensive.annotations.OnValue;
import ru.abdyabdya.intensive.read.dto.ClientDto;
import ru.abdyabdya.intensive.read.dto.ClientInformationDto;
import ru.abdyabdya.intensive.read.dto.ContactDto;

@Component
public class ClientEnumMappedProcessor extends EnumMappedProcessorService<ClientDto> {

    @OnValue("NAME")
    public void updateName(ClientDto toUpdate, ClientInformationDto updateData){
        toUpdate.getClientInformationDto().setName(updateData.getName());
    }

    @OnValue("CONTACT")
    public void updateContacts(ClientDto toUpdate, ClientInformationDto updateData){
        for (ContactDto newContact : updateData.getContacts()) {
            ContactDto oldContact = toUpdate.getClientInformationDto().getContacts().stream()
                    .filter(newContact::equals)
                    .findFirst().orElse(null);
            if (oldContact != null) {
                oldContact.setValue(newContact.getValue());
            } else {
                toUpdate.getClientInformationDto().getContacts().add(newContact);
            }
        }
    }

    @OnValue("PAYMENT_TYPE")
    public void updatePaymentTypes(ClientDto toUpdate, ClientInformationDto updateData){
        toUpdate.getClientInformationDto().setPaymentTypes(updateData.getPaymentTypes());
    }
}
