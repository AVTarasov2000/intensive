package ru.abdyabdya.intensive.read.dto;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class ClientDto {
    private Long eventId;
    private ClientInformationDto clientInformationDto;
}
