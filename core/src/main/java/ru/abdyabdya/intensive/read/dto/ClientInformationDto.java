package ru.abdyabdya.intensive.read.dto;

import lombok.Builder;
import lombok.Data;
import ru.abdyabdya.intensive.model.PaymentType;

import java.util.List;
import java.util.Set;


@Data
@Builder
public class ClientInformationDto {
    private String name;
    private List <ContactDto> contacts;
    private Set <PaymentType> paymentTypes;
}
