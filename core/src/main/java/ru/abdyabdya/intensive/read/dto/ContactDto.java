package ru.abdyabdya.intensive.read.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.abdyabdya.intensive.wright.model.contact.ContactType;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ContactDto {
    private UUID id;
    @EqualsAndHashCode.Include
    private ContactType type;
    @EqualsAndHashCode.Include
    private String value;
}
