package ru.abdyabdya.intensive.read.fasade;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.abdyabdya.intensive.read.appliers.ClientApplier;
import ru.abdyabdya.intensive.read.dto.ClientDto;
import ru.abdyabdya.intensive.read.mapper.ClientInformationReadMapper;
import ru.abdyabdya.intensive.read.mapper.ClientReadMapper;
import ru.abdyabdya.intensive.read.repository.ClientReadRepository;
import ru.abdyabdya.intensive.repository.EventRepository;

@Service
@RequiredArgsConstructor
public class ClientApplierFacade {

    private final ClientApplier clientApplier;
    private final ClientReadRepository clientReadRepository;
    private final EventRepository eventRepository;
    private final ClientReadMapper clientReadMapper;

    public ClientDto getClient(long eventId){
        return clientApplier.apply(
                clientReadRepository.findById(eventId).map(clientReadMapper::toDto).orElse(null),
                eventRepository.findAllByEventIdOrderByCreateAtAsc(eventId));
    }
}
