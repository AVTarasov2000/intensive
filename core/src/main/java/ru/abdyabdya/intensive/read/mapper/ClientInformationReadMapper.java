package ru.abdyabdya.intensive.read.mapper;

import org.mapstruct.Mapper;
import ru.abdyabdya.intensive.mapper.config.IgnoreUnmappedMapperConfig;
import ru.abdyabdya.intensive.read.dto.ClientInformationDto;
import ru.abdyabdya.intensive.read.model.ClientInformationRead;

@Mapper(config = IgnoreUnmappedMapperConfig.class,
        uses = {ContactReadMapper.class})
public interface ClientInformationReadMapper {
    ClientInformationDto toDto(ClientInformationRead clientInformation);

}
