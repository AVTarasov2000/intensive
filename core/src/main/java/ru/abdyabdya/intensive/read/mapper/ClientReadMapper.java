package ru.abdyabdya.intensive.read.mapper;

import org.mapstruct.Mapper;
import ru.abdyabdya.intensive.mapper.config.IgnoreUnmappedMapperConfig;
import ru.abdyabdya.intensive.read.dto.ClientDto;
import ru.abdyabdya.intensive.read.model.ClientRead;

@Mapper(config = IgnoreUnmappedMapperConfig.class,
        uses = {ClientInformationReadMapper.class})
public interface ClientReadMapper {
    ClientDto toDto(ClientRead clientInformation);

}
