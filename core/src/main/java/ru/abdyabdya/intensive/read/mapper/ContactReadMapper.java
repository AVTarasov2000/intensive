package ru.abdyabdya.intensive.read.mapper;

import org.mapstruct.Mapper;
import ru.abdyabdya.intensive.mapper.config.IgnoreUnmappedMapperConfig;
import ru.abdyabdya.intensive.read.dto.ContactDto;
import ru.abdyabdya.intensive.read.model.ContactRead;
import ru.abdyabdya.intensive.wright.model.contact.Contact;

@Mapper(config = IgnoreUnmappedMapperConfig.class)
public interface ContactReadMapper {
    ContactDto toDto(ContactRead clientInformation);

}
