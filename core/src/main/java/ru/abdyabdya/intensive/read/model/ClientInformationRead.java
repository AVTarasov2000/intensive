package ru.abdyabdya.intensive.read.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.abdyabdya.intensive.hibernate.SetStringPaymentTypeConverter;
import ru.abdyabdya.intensive.model.PaymentType;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Data
@Builder
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.read")
@AllArgsConstructor
@NoArgsConstructor
public class ClientInformationRead {
    @Id
    private String name;
    @OneToMany
    private List <ContactRead> contactReads;
    @Convert(converter = SetStringPaymentTypeConverter.class)
    @Builder.Default
    private Set <PaymentType> paymentTypes = new HashSet<>();
}
