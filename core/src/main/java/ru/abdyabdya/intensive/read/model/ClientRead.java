package ru.abdyabdya.intensive.read.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Data
@Builder
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.read")
@NoArgsConstructor
@AllArgsConstructor
public class ClientRead {
    @Id
    private Long eventId;
    @OneToOne
    @JoinColumn(name = "client_id")
    private ClientInformationRead clientInformationReadDto;
}
