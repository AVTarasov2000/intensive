package ru.abdyabdya.intensive.read.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.abdyabdya.intensive.wright.model.contact.ContactType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.read")
public class ContactRead {
    @Id
    private UUID id;
    @Column
    private ContactType type;
    @Column
    private String value;
}
