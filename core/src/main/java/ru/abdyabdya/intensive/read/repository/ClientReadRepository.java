package ru.abdyabdya.intensive.read.repository;

import org.springframework.data.repository.CrudRepository;
import ru.abdyabdya.intensive.read.model.ClientRead;

public interface ClientReadRepository extends CrudRepository<ClientRead, Long> {
}
