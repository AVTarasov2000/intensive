package ru.abdyabdya.intensive.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.abdyabdya.intensive.model.ApplicationUser;

import java.util.UUID;

public interface ApplicationUserRepository extends PagingAndSortingRepository <ApplicationUser, UUID> {

}
