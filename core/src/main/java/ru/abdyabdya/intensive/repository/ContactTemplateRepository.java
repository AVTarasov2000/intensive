package ru.abdyabdya.intensive.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.abdyabdya.intensive.model.ApplicationUser;
import ru.abdyabdya.intensive.model.ContactTemplate;

import java.util.List;
import java.util.UUID;

public interface ContactTemplateRepository extends PagingAndSortingRepository <ContactTemplate, UUID> {
    List<ContactTemplate> findAll();
}
