package ru.abdyabdya.intensive.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.abdyabdya.intensive.wright.events.Event;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EventRepository extends CrudRepository<Event, UUID> {

    @Query(value = "SELECT nextval(:sequenceName)", nativeQuery = true)
    Long getNextEventId(@Param("sequenceName") String sequenceName);
    List <Event> findAllByEventIdOrderByCreateAtAsc(Long eventId);
    List <Event> findAllByEventIdAndCreateAtAfterOrderByCreateAtAsc(Long eventId, Instant lastDate);
    List <Event> findAllByEventIdInOrderByCreateAtAsc(List<Long> eventId);
    List <Event> findAllByEventIdInAndCreateAtAfterOrderByCreateAtAsc(List<Long> eventId, Instant lastDate);
}
