package ru.abdyabdya.intensive.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.abdyabdya.intensive.model.ApplicationUser;
import ru.abdyabdya.intensive.repository.ApplicationUserRepository;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ApplicationUserService {

    private final ApplicationUserRepository applicationUserRepository;

    public ApplicationUser newClient(ApplicationUser user){
        return applicationUserRepository.save(user);
    }

    public ApplicationUser updateClient(ApplicationUser user){
        if(user.getId() == null){
            throw new IllegalArgumentException("id is required");
        }
        return applicationUserRepository.save(user);
    }

    public void deleteClient(UUID id){
        applicationUserRepository.deleteById(id);
    }
}
