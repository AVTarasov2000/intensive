package ru.abdyabdya.intensive.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.CommandProcessor;
import ru.abdyabdya.intensive.CommandService;
import ru.abdyabdya.intensive.repository.EventRepository;
import ru.abdyabdya.intensive.wright.events.Event;

import java.time.Instant;
import java.util.List;

import static java.util.Collections.emptyList;

@Component
@RequiredArgsConstructor
@Slf4j
public class CommandServiceImpl implements CommandService<Long> {

    private final EventRepository eventRepository;

    @Override
    public List<? extends Command> getCommandsByIdAndDate(Long id, Instant lastAppliedEventCreateDate) {
        return eventRepository.findAllByEventIdAndCreateAtAfterOrderByCreateAtAsc(id, lastAppliedEventCreateDate);
    }

    @Override
    public List<Event> getCommandsById(Long id) {
        return eventRepository.findAllByEventIdOrderByCreateAtAsc(id);
    }
}