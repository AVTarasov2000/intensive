package ru.abdyabdya.intensive.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.abdyabdya.intensive.model.ApplicationUser;
import ru.abdyabdya.intensive.model.ContactTemplate;
import ru.abdyabdya.intensive.repository.ContactTemplateRepository;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ContactTemplateService {

    private final ContactTemplateRepository contactTemplate;

    public List<ContactTemplate> getAll() {
        return contactTemplate.findAll();
    }
}
