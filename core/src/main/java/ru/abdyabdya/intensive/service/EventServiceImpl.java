package ru.abdyabdya.intensive.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.abdyabdya.intensive.ApplierEventService;
import ru.abdyabdya.intensive.ApplyingObject;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.repository.EventRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements ApplierEventService<Long> {
    private final EventRepository eventRepository;

    @Override
    public List<? extends Command> findAllByEventId(ApplyingObject<Long> object) {
        return eventRepository.findAllByEventIdAndCreateAtAfterOrderByCreateAtAsc(object.getId(), object.getLastDate());
    }
}
