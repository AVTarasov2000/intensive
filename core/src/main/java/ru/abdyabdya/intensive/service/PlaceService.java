package ru.abdyabdya.intensive.service;

public interface PlaceService {

    void reservePlace(String code);
    void changeReservation(String prewCode, String nextCode);
    void cancelReservation(String code);
    void attachPlace(String code);
    boolean checkIsPlaceOccupied(String code);
}
