package ru.abdyabdya.intensive.service;

import org.springframework.stereotype.Service;

@Service
public class PlaceServiceImpl implements PlaceService { //todo
    @Override
    public void reservePlace(String code) {

    }

    @Override
    public void changeReservation(String prewCode, String nextCode) {

    }

    @Override
    public void cancelReservation(String code) {

    }

    @Override
    public void attachPlace(String code) {

    }

    @Override
    public boolean checkIsPlaceOccupied(String code) {
        return true;
    }
}
