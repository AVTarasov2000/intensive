package ru.abdyabdya.intensive.service;

import ru.abdyabdya.intensive.wright.events.remind.AddReminding;
import ru.abdyabdya.intensive.wright.events.remind.RepeatReminding;
import ru.abdyabdya.intensive.wright.events.remind.StopReminding;


public interface RemindingService {

    void remind(AddReminding command) ;
    void remind(RepeatReminding command) ;
    void remind(StopReminding command) ;
}
