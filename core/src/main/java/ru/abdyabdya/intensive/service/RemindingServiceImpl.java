package ru.abdyabdya.intensive.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.abdyabdya.intensive.Publisher;
import ru.abdyabdya.intensive.wright.events.remind.AddReminding;
import ru.abdyabdya.intensive.wright.events.remind.RepeatReminding;
import ru.abdyabdya.intensive.wright.events.remind.StopReminding;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
@Slf4j
@RequiredArgsConstructor
public class RemindingServiceImpl implements RemindingService {

    private final Publisher publisher;
    private volatile boolean todoIsRemind = true; //todo

    public void remind(AddReminding command) {
        //todo
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future future = executor.submit(sendReminding(command));
        try {
            future.get(1, TimeUnit.MINUTES);//todo
        } catch (TimeoutException e) {
            future.cancel(true);
        } catch (Exception e) {
            // handle other exceptions
        } finally {
            executor.shutdownNow();
        }
    }

    public void remind(RepeatReminding command) {
        //todo
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future future = executor.submit(sendReminding(command));
        try {
            future.get(1, TimeUnit.MINUTES);//todo
        } catch (TimeoutException e) {
            future.cancel(true);
        } catch (Exception e) {
            // handle other exceptions
        } finally {
            executor.shutdownNow();
        }
    }

    public void remind(StopReminding command) {
        //todo
        todoIsRemind = false;
    }

    private Callable<Void> sendReminding(AddReminding command) {
        return () -> {
            if (todoIsRemind) {
                log.info(command.getMessage());
                publisher.publish(RepeatReminding.builder()
                        .message(command.getMessage())
                        .applicationUser(command.getApplicationUser())
                        .eventId(command.getEventId())
                        .build());
            }
            return null;
        };
    }
    private Callable<Void> sendReminding(RepeatReminding command) {
        return () -> {
            if (todoIsRemind) {
                log.info("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                publisher.publish(RepeatReminding.builder()
                        .message(command.getMessage())
                        .applicationUser(command.getApplicationUser())
                        .eventId(command.getEventId())
                        .build());
            }
            return null;
        };
    }
}
