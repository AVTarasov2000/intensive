package ru.abdyabdya.intensive.wright.events;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.model.ApplicationUser;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

import static javax.persistence.InheritanceType.JOINED;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@SuperBuilder
@NoArgsConstructor
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
@Inheritance(strategy = JOINED)
public abstract class Event implements Command {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Include
    private UUID id;
    @Column
    private Long eventId;
    @Column
    @Builder.Default
    private Instant createAt = Instant.now();
    @ManyToOne
    @JoinColumn(name="user_id")
    private ApplicationUser applicationUser;
}
