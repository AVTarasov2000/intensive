package ru.abdyabdya.intensive.wright.events.client;

public enum ClientField {
    NAME,
    CONTACT,
    PAYMENT_TYPE
}
