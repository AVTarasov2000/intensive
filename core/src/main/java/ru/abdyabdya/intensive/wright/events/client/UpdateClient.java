package ru.abdyabdya.intensive.wright.events.client;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.abdyabdya.intensive.hibernate.SetStringClientFieldsConverter;
import ru.abdyabdya.intensive.wright.model.contact.ClientInformation;
import ru.abdyabdya.intensive.wright.events.Event;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.util.HashSet;
import java.util.Set;
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
public class UpdateClient extends Event {
    @OneToOne(cascade = CascadeType.ALL)
    private ClientInformation clientInformation;
    @Convert(converter = SetStringClientFieldsConverter.class)
    @Builder.Default
    private Set<ClientField> clientFields = new HashSet<>();

}
