package ru.abdyabdya.intensive.wright.events.payment;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.abdyabdya.intensive.wright.events.Event;
import ru.abdyabdya.intensive.model.PaymentType;

import javax.persistence.*;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
public class AcceptPayment extends Event {

    @Column
    private Long amount;
    @Column
    private PaymentType type;
    @Column
    private UUID clientId;
}
