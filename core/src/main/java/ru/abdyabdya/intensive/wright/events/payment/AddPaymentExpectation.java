package ru.abdyabdya.intensive.wright.events.payment;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.abdyabdya.intensive.wright.events.Event;
import ru.abdyabdya.intensive.model.PaymentType;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
public class AddPaymentExpectation extends Event {
    @Column
    @Enumerated(EnumType.STRING)
    private PaymentType type;
}
