package ru.abdyabdya.intensive.wright.events.remind;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.abdyabdya.intensive.wright.events.Event;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Instant;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
public class AddReminding extends Event {
    @Column
    private String message;
    @Column
    private Instant remindAt;
}
