package ru.abdyabdya.intensive.wright.handlers;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.wright.events.client.DeleteClient;
import ru.abdyabdya.intensive.wright.events.client.UpdateClient;
import ru.abdyabdya.intensive.wright.events.payment.AddPaymentExpectation;
import ru.abdyabdya.intensive.wright.events.client.CreateClient;
import ru.abdyabdya.intensive.annotations.Handler;
import ru.abdyabdya.intensive.model.PaymentType;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Component
@Handler
@Slf4j
public class ClientHandler {

    @Handler
    public List<Command> handle(CreateClient command) {
        return createPaymentCommands(command);
    }

    @Handler
    public List<Command> handle(UpdateClient command) {
        return emptyList();
    }

    @Handler
    public List<Command> handle(DeleteClient command) {
        return emptyList();
    }

    private List <Command> createPaymentCommands(CreateClient command){
        List<Command> list = new LinkedList();
        list.add(AddPaymentExpectation.builder()
                        .eventId(command.getEventId())
                        .applicationUser(command.getApplicationUser())
                        .type(PaymentType.PARTICIPATION).build());
        list.addAll(command.getClientInformation().getPaymentTypes().stream().map(type->
            AddPaymentExpectation.builder()
                    .eventId(command.getEventId())
                    .applicationUser(command.getApplicationUser())
                    .type(type).build()
        ).collect(Collectors.toList()));
        return list;
    }

}
