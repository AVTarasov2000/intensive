package ru.abdyabdya.intensive.wright.handlers;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.annotations.Handler;
import ru.abdyabdya.intensive.exception.PlaceIsAlreadyReservedException;
import ru.abdyabdya.intensive.service.PlaceService;
import ru.abdyabdya.intensive.wright.events.housing.AttachPlace;
import ru.abdyabdya.intensive.wright.events.housing.CancelPlaceReserving;
import ru.abdyabdya.intensive.wright.events.housing.ChangePlaceReserving;
import ru.abdyabdya.intensive.wright.events.housing.ReservePlace;
import ru.abdyabdya.intensive.wright.events.remind.AddReminding;
import ru.abdyabdya.intensive.wright.events.remind.StopReminding;

import java.util.List;

@Component
@Handler
@Slf4j
@RequiredArgsConstructor
public class HousingHandler {

    private final PlaceService placeService;

    @Handler
    public List<Command> handle(ReservePlace command) {
        if (!placeService.checkIsPlaceOccupied(command.getPlaceCode())) {
            placeService.reservePlace(command.getPlaceCode());
            return List.of(AddReminding.builder()
                    .eventId(command.getEventId())
                    .applicationUser(command.getApplicationUser()).build());
        } else {
            throw new PlaceIsAlreadyReservedException(command.getPlaceCode());
        }
    }

    @Handler
    public List<Command> handle(AttachPlace command) {
        if (!placeService.checkIsPlaceOccupied(command.getPlaceCode())) {
            placeService.attachPlace(command.getPlaceCode());
            return List.of(StopReminding.builder()
                    .eventId(command.getEventId())
                    .applicationUser(command.getApplicationUser()).build());
        } else {
            throw new PlaceIsAlreadyReservedException(command.getPlaceCode());
        }
    }

    @Handler
    public List<Command> handle(ChangePlaceReserving command) {
        if (!placeService.checkIsPlaceOccupied(command.getNewPlaceCode())) {
            placeService.changeReservation(command.getOldPlaceCode(), command.getNewPlaceCode());
            return List.of(StopReminding.builder()
                            .eventId(command.getEventId())
                            .applicationUser(command.getApplicationUser()).build(),
                    AddReminding.builder()
                            .eventId(command.getEventId())
                            .applicationUser(command.getApplicationUser()).build());
        } else {
            throw new PlaceIsAlreadyReservedException(command.getNewPlaceCode());
        }
    }

    @Handler
    public List<Command> handle(CancelPlaceReserving command) {
        if (!placeService.checkIsPlaceOccupied(command.getPlaceCode())) {
            placeService.cancelReservation(command.getPlaceCode());
            return List.of(StopReminding.builder()
                    .eventId(command.getEventId())
                    .applicationUser(command.getApplicationUser()).build());
        } else {
            throw new PlaceIsAlreadyReservedException(command.getPlaceCode());
        }
    }
}
