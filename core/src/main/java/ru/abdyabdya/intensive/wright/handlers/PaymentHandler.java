package ru.abdyabdya.intensive.wright.handlers;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.annotations.Handler;
import ru.abdyabdya.intensive.wright.events.payment.AcceptPayment;
import ru.abdyabdya.intensive.wright.events.payment.AddPaymentExpectation;
import ru.abdyabdya.intensive.wright.events.remind.AddReminding;
import ru.abdyabdya.intensive.wright.events.remind.StopReminding;

import java.util.List;

@Component
@Handler
@Slf4j
public class PaymentHandler {
    @Handler
    public List<Command> handle(AddPaymentExpectation command){
        return List.of(AddReminding.builder()
                .eventId(command.getEventId())
                .applicationUser(command.getApplicationUser()).build());
    }

    @Handler
    public List<Command> handle(AcceptPayment command){
        return List.of(StopReminding.builder()
                .eventId(command.getEventId())
                .applicationUser(command.getApplicationUser()).build());
    }
}
