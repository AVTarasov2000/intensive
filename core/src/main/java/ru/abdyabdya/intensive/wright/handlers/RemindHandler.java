package ru.abdyabdya.intensive.wright.handlers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.annotations.Handler;
import ru.abdyabdya.intensive.service.RemindingService;
import ru.abdyabdya.intensive.wright.events.remind.AddReminding;
import ru.abdyabdya.intensive.wright.events.remind.RepeatReminding;
import ru.abdyabdya.intensive.wright.events.remind.StopReminding;

import java.util.Collections;
import java.util.List;

@Component
@Handler
@Slf4j
@RequiredArgsConstructor
public class RemindHandler {

    private final RemindingService remindingService;

    @Handler
    public List<Command> handle(AddReminding command){
        remindingService.remind(command);
        return Collections.emptyList();
    }
    @Handler
    public List<Command> handle(StopReminding command){
//        remindingService.remind(command);
        return Collections.emptyList();
    }

    @Handler
    public List<Command> handle(RepeatReminding command){
//        remindingService.remind(command);
        return Collections.emptyList();
    }

}
