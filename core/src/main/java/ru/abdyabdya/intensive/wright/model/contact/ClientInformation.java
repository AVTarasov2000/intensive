package ru.abdyabdya.intensive.wright.model.contact;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.abdyabdya.intensive.hibernate.SetStringPaymentTypeConverter;
import ru.abdyabdya.intensive.model.PaymentType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.AUTO;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
public class ClientInformation {
    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;
    @Column
    private String name;
    @OneToMany(cascade=ALL)
    private List <Contact> contacts;
    @Convert(converter = SetStringPaymentTypeConverter.class)
    @Builder.Default
    private Set <PaymentType> paymentTypes = new HashSet <>();
}
