package ru.abdyabdya.intensive.wright.model.contact;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
public class Contact {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;
    @Column
    @Enumerated(EnumType.STRING)
    private ContactType type;
    @Column
    private String value;

}
