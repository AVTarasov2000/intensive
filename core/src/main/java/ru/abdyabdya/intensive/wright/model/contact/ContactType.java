package ru.abdyabdya.intensive.wright.model.contact;

public enum ContactType {
    EMAIL,
    PHONE,
    VK,
    TELEGRAM,
    INSTAGRAM,
    FACEBOOK,
    DISCORD,
    SKYPE,
    WHATS_APP,
    TEAMS,
    OTHER
}
