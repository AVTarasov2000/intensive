package ru.abdyabdya.intensive.wright.model.payment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "${spring.jpa.properties.hibernate.default_schema}.wright")
public class Payment {
    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;
    @Column
    private String type;
    @Column
    private Long amount;
}
