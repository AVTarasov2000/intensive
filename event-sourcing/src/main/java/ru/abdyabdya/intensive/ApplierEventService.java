package ru.abdyabdya.intensive;

import java.util.List;

public interface ApplierEventService<T> {
    List<? extends Command> findAllByEventId(ApplyingObject<T> object);
}
