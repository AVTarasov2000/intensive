package ru.abdyabdya.intensive;

public interface ApplierFunction<T> {
    public T apply(T object, Command event);
}
