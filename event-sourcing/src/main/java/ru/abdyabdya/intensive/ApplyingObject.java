package ru.abdyabdya.intensive;

import java.time.Instant;

public interface ApplyingObject<I> {
    I getId();
    Instant getLastDate();
}
