package ru.abdyabdya.intensive;

public interface CommandProcessor {
    Command processCommand(Command command);
    Command processFailure(Command command);
}
