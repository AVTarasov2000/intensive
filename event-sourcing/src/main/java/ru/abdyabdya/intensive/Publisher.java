package ru.abdyabdya.intensive;

import java.util.List;
import java.util.function.Consumer;

public interface Publisher {
    Command publish(Command message);
    void publish(List<Command> proceed);
    Command fail(Command message);
    void fail(List<Command> proceed);
    void addCommandHandler(Class type, Consumer<Command> consumer);
}
