package ru.abdyabdya.intensive;

import org.springframework.stereotype.Component;

@Component
public interface SnapshotService {

    void takeSnapshot(Long id);

}
