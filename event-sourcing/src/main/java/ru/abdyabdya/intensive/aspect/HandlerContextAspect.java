package ru.abdyabdya.intensive.aspect;

import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.abdyabdya.intensive.Command;
import ru.abdyabdya.intensive.errors.CommandException;
import ru.abdyabdya.intensive.Publisher;

import java.util.List;

@Aspect
@Component
public class HandlerContextAspect {

    @Autowired
    private Publisher publisher;

    @Pointcut(value = "@annotation(ru.abdyabdya.intensive.annotations.Handler)")
    public void callAtHandlerAnnotation() { }

    @Around(value = "callAtHandlerAnnotation()")
    @SneakyThrows
    public Object aroundCallAt(ProceedingJoinPoint pjp) {
        List<Command> retVal = null;
        try {
            retVal = (List<Command>)pjp.proceed();
            publisher.publish(retVal);
        } catch (CommandException commandException) {
            publisher.fail(commandException.getCommand());
        }
        return retVal;
    }
}
