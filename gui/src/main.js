import { createApp } from 'vue'
import App from './App.vue'
import Vue3Material from 'vue3-material';
import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

createApp(App)
    .use(Vue3Material)
    .mount('#app')
