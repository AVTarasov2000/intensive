let contactTypes = [{value: 'EMAIL'},
    {value: 'PHONE'},
    {value: 'VK'},
    {value: 'TELEGRAM'},
    {value: 'INSTAGRAM'},
    {value: 'FACEBOOK'},
    {value: 'DISCORD'},
    {value: 'SKYPE'},
    {value: 'WHATS_APP'},
    {value: 'TEAMS'},
    {value: 'OTHER'}]
export default contactTypes;