const foodTypes = [
    {code: "BLD", value: "завтрак, обед, ужин"},
    {code: "BL", value: "завтрак, обед"},
    {code: "BD", value: "завтрак, ужин"},
    {code: "LD", value: "обед, ужин"},
    {code: "B", value: "завтрак"},
    {code: "L", value: "обед"},
    {code: "D", value: "ужин"},
    {code: "N", value: "буз еды"},

]
export default foodTypes;