export class Contact {
    isUpdating = false;
    showButtons = false;
    id;
    type;
    value;

    constructor(id, type, value) {
        this.id = id;
        this.type = type;
        this.value = value;
    }
}