const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: '/intensive',
  devServer: {
    proxy: 'http://localhost:8080'
  },
  transpileDependencies: true
})
