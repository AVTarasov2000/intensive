CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE SEQUENCE event_id START 2;

CREATE TABLE application_user
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    email character varying(255) COLLATE pg_catalog."default",
    login character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    phone character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT application_user_pkey PRIMARY KEY (id)
) TABLESPACE pg_default;

CREATE TABLE event
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    create_at timestamp without time zone NOT NULL DEFAULT now()::timestamp,
    event_id bigint,
    user_id uuid,
    CONSTRAINT event_pkey PRIMARY KEY (id),
    CONSTRAINT fk6sjukucbjt3ys5p968omujq68 FOREIGN KEY (user_id)
        REFERENCES application_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE contact
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    type character varying(255) COLLATE pg_catalog."default",
    value character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT contact_pkey PRIMARY KEY (id)
) TABLESPACE pg_default;

CREATE TABLE accept_payment
(
    amount bigint,
    client_id uuid,
    type integer,
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT accept_payment_pkey PRIMARY KEY (id),
    CONSTRAINT fknbis7dxw6onr61o115aufk4q4 FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE add_payment_expectation
(
    type character varying(255) COLLATE pg_catalog."default",
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT add_payment_expectation_pkey PRIMARY KEY (id),
    CONSTRAINT fk1u41vpffko81siia7biht8mx FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE add_reminding
(
    message character varying(255) COLLATE pg_catalog."default",
    remind_at timestamp without time zone,
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT add_reminding_pkey PRIMARY KEY (id),
    CONSTRAINT fkbkla42krnacmslxkp6pmbgkf4 FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE attach_place
(
    place_code character varying(255) COLLATE pg_catalog."default",
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT attach_place_pkey PRIMARY KEY (id),
    CONSTRAINT fkml9fq1d6xqfk570uoe0kmu0j7 FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE cancel_place_reserving
(
    place_code character varying(255) COLLATE pg_catalog."default",
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT cancel_place_reserving_pkey PRIMARY KEY (id),
    CONSTRAINT fk126ic7r6ytx1dv8qke8hmbkva FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE change_place_reserving
(
    new_place_code character varying(255) COLLATE pg_catalog."default",
    old_place_code character varying(255) COLLATE pg_catalog."default",
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT change_place_reserving_pkey PRIMARY KEY (id),
    CONSTRAINT fkkfcciwuipx64igl4i6ay1d4qw FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE client_information
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    name character varying(255) COLLATE pg_catalog."default",
    payment_types character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT client_information_pkey PRIMARY KEY (id)
) TABLESPACE pg_default;

CREATE TABLE client_information_contacts
(
    client_information_id uuid NOT NULL DEFAULT uuid_generate_v4(),
    contacts_id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT uk_69k2ijm8boxxde49a5kx806gg UNIQUE (contacts_id),
    CONSTRAINT fk2jtnr7qst5xvklvtubmbcfdl8 FOREIGN KEY (client_information_id)
        REFERENCES client_information (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkjofh2ea294c8kevdckwg6akoh FOREIGN KEY (contacts_id)
        REFERENCES contact (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE contact_validator
(
    type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    regexp character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT contact_validator_pkey PRIMARY KEY (type)
) TABLESPACE pg_default;

CREATE TABLE create_client
(
    name character varying(255) COLLATE pg_catalog."default",
    payment_types character varying(255) COLLATE pg_catalog."default",
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    client_information_id uuid NOT NULL,
    CONSTRAINT create_client_pkey PRIMARY KEY (id),
    CONSTRAINT fk4eaivlfs5f0php8lhc9sqfguo FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE create_client_contacts
(
    create_client_id uuid NOT NULL DEFAULT uuid_generate_v4(),
    contacts_id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT uk_2jybtsa9xx6hmf334jhggmf7y UNIQUE (contacts_id),
    CONSTRAINT fk3gd4jcfofpymx6xfdyqjd4m64 FOREIGN KEY (contacts_id)
        REFERENCES contact (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fktndfrpp4ondcclffmiokpew0s FOREIGN KEY (create_client_id)
        REFERENCES create_client (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE delete_client
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT delete_client_pkey PRIMARY KEY (id),
    CONSTRAINT fk6e69qnx1ctxy4bwk10rjh03q2 FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE payment
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    amount bigint,
    type character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT payment_pkey PRIMARY KEY (id)
) TABLESPACE pg_default;

CREATE TABLE repeat_reminding
(
    message character varying(255) COLLATE pg_catalog."default",
    remind_at timestamp without time zone,
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT repeat_reminding_pkey PRIMARY KEY (id),
    CONSTRAINT fkl7qfyqdu63tk5l4eqch9yc6mx FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE reserve_place
(
    place_code character varying(255) COLLATE pg_catalog."default",
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT reserve_place_pkey PRIMARY KEY (id),
    CONSTRAINT fkngep1gxoua193hfjsrwld6yeh FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE stop_reminding
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT stop_reminding_pkey PRIMARY KEY (id),
    CONSTRAINT fk3ulgbfa404w0lmucbq37rr2q9 FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE update_client
(
    name character varying(255) COLLATE pg_catalog."default",
    payments character varying(255) COLLATE pg_catalog."default",
    client_fields character varying(255) COLLATE pg_catalog."default",
    client_information_id uuid NOT NULL,
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT update_client_pkey PRIMARY KEY (id),
    CONSTRAINT fkrthxl6cf9ljpp6avqu04isb1b FOREIGN KEY (id)
        REFERENCES event (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;

CREATE TABLE update_client_contacts
(
    update_client_id uuid NOT NULL DEFAULT uuid_generate_v4(),
    contacts_id uuid NOT NULL DEFAULT uuid_generate_v4(),
    CONSTRAINT uk_f3rk7igcd2e79bii12j22pnwv UNIQUE (contacts_id),
    CONSTRAINT fknoloqgo9rg7m34n3xr4s5nshk FOREIGN KEY (update_client_id)
        REFERENCES update_client (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fksu1uplyp432u65qnf5wrc6y2 FOREIGN KEY (contacts_id)
        REFERENCES contact (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) TABLESPACE pg_default;
