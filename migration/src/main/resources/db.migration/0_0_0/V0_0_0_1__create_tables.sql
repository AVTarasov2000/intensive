CREATE TABLE contact_template
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    value character varying(255) COLLATE pg_catalog."default",
    regexp character varying(255) COLLATE pg_catalog."default",
    example character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT contact_template_pkey PRIMARY KEY (id)
) TABLESPACE pg_default;

insert into contact_template (value,regexp,example)
values ('EMAIL',
        '^((\w[^\W]+)[\.\-]?){1,}\@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$',
        'ivanov@mail.ru');

insert into contact_template (value,regexp,example) values ('PHONE','^(8|\+7)(\d{10}|(-?\d{3}-?\d{3}-?\d{2}-?\d{2}))$','88888888888');
insert into contact_template (value,regexp,example) values ('VK','*','user');
insert into contact_template (value,regexp,example) values ('TELEGRAM','^(t\.me/|@)(\d|_|-|[a-zA-Z])*$','@user');
insert into contact_template (value,regexp,example) values ('INSTAGRAM','*','user');
insert into contact_template (value,regexp,example) values ('FACEBOOK','*','user');
insert into contact_template (value,regexp,example) values ('DISCORD','*','user');
insert into contact_template (value,regexp,example) values ('SKYPE','*','user');
insert into contact_template (value,regexp,example) values ('WHATS_APP','*','user');
insert into contact_template (value,regexp,example) values ('TEAMS','*','user');
insert into contact_template (value,regexp,example) values ('OTHER','*','user');