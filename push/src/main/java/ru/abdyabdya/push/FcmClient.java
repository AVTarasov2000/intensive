package ru.abdyabdya.push;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushNotification;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.abdyabdya.push.dto.PushNotificationContentDto;

import java.util.Collections;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class FcmClient {


    public static final String TTL = "ttl";
    private final FirebaseMessaging firebaseMessaging;

    @SneakyThrows
    public String sendByTopic(PushNotificationContentDto conf, String topic) {
        Message message = Message.builder().setTopic(topic)
                .setWebpushConfig(WebpushConfig.builder()
                        .putHeader(TTL, conf.getTtlInSeconds())
                        .setNotification(creatWebpushNotification(conf))
                        .build())
                .build();

        return firebaseMessaging.sendAsync(message).get();
    }

    @SneakyThrows
    public String sendPersonal(PushNotificationContentDto conf, String clientToken) {
        Message message = Message.builder().setToken(clientToken)
                .setWebpushConfig(WebpushConfig.builder()
                        .putHeader(TTL, conf.getTtlInSeconds())
                        .setNotification(creatWebpushNotification(conf))
                        .build())
                .build();

        return firebaseMessaging.sendAsync(message).get();
    }

    @SneakyThrows
    public void subscribeUsers(String topic, List<String> clientTokens) {
        for (String token : clientTokens) {
            firebaseMessaging.subscribeToTopic(Collections.singletonList(token), topic);
        }
    }

    private WebpushNotification creatWebpushNotification(PushNotificationContentDto conf){
        return WebpushNotification.builder().addAction(new WebpushNotification
                .Action(conf.getClick_action(), "Открыть"))
                .setImage(conf.getIcon())
                .setTitle(conf.getTitle())
                .setBody(conf.getBody()).build();
    }
}
