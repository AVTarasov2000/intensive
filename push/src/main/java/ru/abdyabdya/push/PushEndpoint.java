package ru.abdyabdya.push;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.abdyabdya.push.dto.PushNotificationContentDto;
import ru.abdyabdya.push.dto.SubscribeDto;

import static java.util.List.of;

@RestController
@RequiredArgsConstructor
@RequestMapping("/send")
public class PushEndpoint {

    private final FcmClient fcmClient;

    @PostMapping
    public ResponseEntity<Void> newClient(){
        fcmClient.subscribeUsers("test", of("test"));
        return ResponseEntity.ok().build();
    }

    @PostMapping("/topic/{topic}")
    public String sendByTopic(
            @RequestBody PushNotificationContentDto conf,
            @PathVariable String topic) {
        return fcmClient.sendByTopic(conf, topic);
    }

    @PostMapping("/personal/{clientToken}")
    public String sendPersonal(
            @RequestBody PushNotificationContentDto conf,
            @PathVariable String clientToken) {
        return fcmClient.sendPersonal(conf, clientToken);
    }

    @PostMapping("/subscribe")
    public void subscribeUsers(@RequestBody SubscribeDto subscribeDto) {
        fcmClient.subscribeUsers(subscribeDto.getTopic(), subscribeDto.getClientTokens());
    }
}
