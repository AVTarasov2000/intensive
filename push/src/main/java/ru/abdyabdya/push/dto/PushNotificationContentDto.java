package ru.abdyabdya.push.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PushNotificationContentDto {
    private String title;
    private String body;
    private String icon;
    private String click_action;
    private String ttlInSeconds;
}
