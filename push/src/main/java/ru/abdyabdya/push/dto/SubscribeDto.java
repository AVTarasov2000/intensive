package ru.abdyabdya.push.dto;

import lombok.Data;

import java.util.List;

@Data
public class SubscribeDto {
    String topic;
    List<String> clientTokens;
}
